include .env
# configurations
SHELL = /bin/zsh
openvpn=docker-compose -f fig.$(ENV).yml

update:
	git pull
	cp .env_ .env

# Compose Not Used

compose-up:
	$(openvpn) up -d

compose-down:
	$(openvpn) down

compose-start:
	$(openvpn) start

compose-stop:
	$(openvpn) stop

compose-pull:
	$(openvpn) pull openvpn
	
compose-reload: openvpn-stop openvpn-start

compose-restart: openvpn-down openvpn-up

compose-status:
	$(openvpn) ps

# First Start

create-volume:
	docker volume create --name ovpn-data-$(VPN_ENV)

remove-volume:
	docker volume rm -f ovpn-data-$(VPN_ENV)

generate-vpn-config:
	docker run -v ovpn-data-$(VPN_ENV):/etc/openvpn --rm  --name $(OPENVPN_CON) $(OPENVPN_IMAGE) ovpn_genconfig -u udp://$(VPN_ADDRESS) 

generate-vpn-pki-cert:
	docker run -v ovpn-data-$(VPN_ENV):/etc/openvpn --rm -it --name $(OPENVPN_CON) $(OPENVPN_IMAGE) ovpn_initpki

generate-service-definition:
	curl -L $(SERVICE_DEFINITION) | sudo tee /etc/systemd/system/docker-openvpn@.service

enable-vpn-as-service:
	systemctl enable --now docker-openvpn@$(VPN_ENV).service

vpn-docker-standalone:
	docker run -v ovpn-data-$(VPN_ENV):/etc/openvpn -d -p $(VPN_PORT):$(VPN_PORT)/udp --cap-add=NET_ADMIN  --name $(OPENVPN_CON) $(OPENVPN_IMAGE)

# Regular Use

vpn-generate-client:
	docker run -v ovpn-data-$(VPN_ENV):/etc/openvpn --log-driver=none --rm -it --name $(OPENVPN_CON) $(OPENVPN_IMAGE) easyrsa build-client-full $(CLIENT_NAME) nopass

vpn-export-client:
	docker run -v ovpn-data-$(VPN_ENV):/etc/openvpn --log-driver=none --rm --name $(OPENVPN_CON) $(OPENVPN_IMAGE) ovpn_getclient $(CLIENT_NAME) > $(CLIENT_NAME).ovpn

vpn-list-client:
	docker run --rm -it -v ovpn-data-$(VPN_ENV):/etc/openvpn --name $(OPENVPN_CON) $(OPENVPN_IMAGE) ovpn_listclients

vpn-generate-all-client:
	docker run --rm -it -v ovpn-data-$(VPN_ENV):/etc/openvpn --volume ./openvpn_clients:/etc/openvpn/clients --name $(OPENVPN_CON) $(OPENVPN_IMAGE) ovpn_getclient_all

vpn-revoke-client:
	docker run --rm -it -v ovpn-data-$(VPN_ENV):/etc/openvpn --name $(OPENVPN_CON) $(OPENVPN_IMAGE) ovpn_revokeclient $(CLIENT_NAME)

vpn-revoke-remove-client:
	docker run --rm -it -v ovpn-data-$(VPN_ENV):/etc/openvpn --name $(OPENVPN_CON) $(OPENVPN_IMAGE) ovpn_revokeclient $(CLIENT_NAME) remove

vpn-start:
	systemctl start docker-openvpn@$(VPN_ENV).service

vpn-status:
	systemctl status docker-openvpn@$(VPN_ENV).service
	journalctl --unit docker-openvpn@$(VPN_ENV).service

vpn-stop:
	systemctl stop docker-openvpn@$(VPN_ENV).service

vpn-restart:
	systemctl restart docker-openvpn@$(VPN_ENV).service

create-vpn-cert: vpn-stop vpn-generate-client vpn-export-client vpn-start

revoke-vpn-cert: vpn-stop vpn-revoke-client vpn-start

remove-vpn-cert: vpn-stop vpn-revoke-remove-client vpn-start
