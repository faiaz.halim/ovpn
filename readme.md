Create VPN for first time (make sure .env file values are correct and unique)

```
make create-volume generate-vpn-config generate-vpn-pki-cert
make generate-service-definition enable-vpn-as-service vpn-start
```

Create new certificate for a client (make sure CLIENT_NAME in .env is unique for each case)

```
make create-vpn-cert
```

Revoke client certificate

```
make revoke-vpn-cert
```

Revoke and remove client certificate

```
make remove-vpn-cert
```

Restart vpn service

```
make vpn-restart
```